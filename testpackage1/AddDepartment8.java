package testpackage1;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddDepartment8 {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();
	@Before
	public void setUp() throws Exception {

		System.setProperty("webdriver.chrome.driver", "D://chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		baseUrl = "https://micloud365.sharepoint.com/sites/appsdevv15";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
}
	@Test
	  public void testUntitledTestCase() throws Exception {
		driver.get(baseUrl+"/FlowF31/Pages/index.aspx");
		
		//sharepoint login
		 driver.findElement(By.id("i0116")).sendKeys("dev-test-02@millenniumitesp.com");

			WebElement a = driver.findElement(By.id("idSIButton9"));
			a.click();
			driver.findElement(By.id("i0118")).sendKeys("e~WR@)<VJ5!q7?2Q");
			WebDriverWait wait1 = new WebDriverWait(driver, 30);
			wait1.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();
			
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();
			
			    driver.findElement(By.linkText("Departments")).click();
			    driver.findElement(By.id("department1Name")).click();
			    driver.findElement(By.id("department1Name")).clear();
			    driver.findElement(By.id("department1Name")).sendKeys(" Dept");
			    
			    driver.findElement(By.id("HODdiv_TopSpan_InitialHelpText")).click();
			    driver.findElement(By.id("HODdiv_TopSpan_EditorInput")).clear();
			    driver.findElement(By.id("HODdiv_TopSpan_EditorInput")).sendKeys("dev-test-03");
			    driver.findElement(By.id("HODdiv_TopSpan_EditorInput")).sendKeys(Keys.ENTER);
			    
			    driver.findElement(By.id("Delegateediv_TopSpan_InitialHelpText")).click();
			    driver.findElement(By.id("Delegateediv_TopSpan_EditorInput")).clear();
			    driver.findElement(By.id("Delegateediv_TopSpan_EditorInput")).sendKeys("dev-test-04");
			    driver.findElement(By.id("Delegateediv_TopSpan_EditorInput")).sendKeys(Keys.ENTER);
			    
			    driver.findElement(By.id("AllUsersdiv_TopSpan_InitialHelpText")).click();
			    driver.findElement(By.id("AllUsersdiv_TopSpan_EditorInput")).clear();
			    driver.findElement(By.id("AllUsersdiv_TopSpan_EditorInput")).sendKeys("Manager");
			    driver.findElement(By.id("AllUsersdiv_TopSpan_EditorInput")).sendKeys(Keys.ENTER);
			    
			    driver.findElement(By.xpath("//*[@id=\"DeltaPlaceHolderMain\"]/div[3]/div[2]/div/div/div/div[1]/span/div/div/div[2]/div/div[6]/div/div[2]/div")).click();
			   // driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Suggestions are available. Use up and down arrows to select.'])[3]/following::div[6]")).click();
			    
			  }
	 @After
	  public void tearDown() throws Exception {
	    driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	      fail(verificationErrorString);
	    }
	  }

	  private boolean isElementPresent(By by) {
	    try {
	      driver.findElement(by);
	      return true;
	    } catch (NoSuchElementException e) {
	      return false;
	    }
	  }

	  private boolean isAlertPresent() {
	    try {
	      driver.switchTo().alert();
	      return true;
	    } catch (NoAlertPresentException e) {
	      return false;
	    }
	  }

	  private String closeAlertAndGetItsText() {
	    try {
	      Alert alert = driver.switchTo().alert();
	      String alertText = alert.getText();
	      if (acceptNextAlert) {
	        alert.accept();
	      } else {
	        alert.dismiss();
	      }
	      return alertText;
	    } finally {
	      acceptNextAlert = true;
	    }
	  }
	}

