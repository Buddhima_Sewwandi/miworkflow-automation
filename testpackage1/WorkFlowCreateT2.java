package testpackage1;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WorkFlowCreateT2 {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		
		Logger logger=Logger.getLogger("log1");
		System.setProperty("webdriver.chrome.driver", "D://chromedriver.exe");
		driver = new ChromeDriver();
		logger.info("Browser Opened");
		
		driver.manage().window().maximize();
		baseUrl = "https://micloud365.sharepoint.com/sites/appsdevv15";
		logger.info("Browser Opened");
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
}
	@Test
	public void testWorkFlowCreate() throws Exception {
		Logger logger=Logger.getLogger("log1");
		// Share point login
		driver.get(baseUrl);
		driver.findElement(By.id("i0116")).sendKeys("dev-test-02@millenniumitesp.com");

		WebElement a = driver.findElement(By.id("idSIButton9"));
		a.click();
		driver.findElement(By.id("i0118")).sendKeys("e~WR@)<VJ5!q7?2Q");
		
		WebDriverWait wait1 = new WebDriverWait(driver, 3000);
		wait1.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();
		logger.info("Explicit wait1");

		WebDriverWait wait2 = new WebDriverWait(driver, 3000);
		wait2.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();
		logger.info("Explicit wait2");

		WebDriverWait wait3 = new WebDriverWait(driver, 3000);
		wait3.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myTab\"]/li[2]/a"))).click();
		logger.info("Explicit wait3");
		
		
		  driver.findElement(By.linkText("Create App")).click();
		    driver.findElement(By.id("wfTitle")).click();
		    driver.findElement(By.id("wfTitle")).clear();
		    driver.findElement(By.id("wfTitle")).sendKeys("FowTest66748550");
		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		    logger.info("Implicit wait_wfTitle");
		    
		  

		    driver.findElement(By.id("workflowName")).click();
		    driver.findElement(By.id("workflowName")).clear();
		    driver.findElement(By.id("workflowName")).sendKeys("FlowTest66748550");
		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		    logger.info("Implicit wait_workflowName");
		    
		    driver.findElement(By.id("wfDescription")).click();
		    driver.findElement(By.id("wfDescription")).clear();
		    driver.findElement(By.id("wfDescription")).sendKeys("FlowTest66748550");
		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		    logger.info("Implicit wait_wfDescription");
		    
		    driver.findElement(By.id("workflowImage")).click();
		    driver.findElement(By.id("workflowImage")).clear();
		    driver.findElement(By.id("workflowImage")).sendKeys("D:\\index.jpg");
		    driver.findElement(By.id("submitWorkflow")).click();
		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		    logger.info("Implicit wait_workflowImage");
		    
		    driver.findElement(By.id("fieldName")).click();
		    driver.findElement(By.id("fieldName")).clear();
		    driver.findElement(By.id("fieldName")).sendKeys("Name");
		    driver.findElement(By.id("fieldType")).click();
		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		    logger.info("Implicit wait_fieldName1");
		    
		    
		    new Select(driver.findElement(By.id("fieldType"))).selectByVisibleText("Text");
		    driver.findElement(By.id("fieldType")).click();
		    driver.findElement(By.id("addButton")).click();
		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		    logger.info("Implicit wait_fieldType1");
		    
		    
		    driver.findElement(By.id("fieldName")).click();
		    driver.findElement(By.id("nextButton")).click();
		    driver.findElement(By.id("NameDefault")).click();
		    driver.findElement(By.id("NameDefault")).clear();
		    driver.findElement(By.id("NameDefault")).sendKeys("Name");
		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		    logger.info("Implicit wait_NameDefault");
		    
		    driver.findElement(By.id("templatebtn1")).click();
		    
		    driver.findElement(By.id("zone1")).click();
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/i[1]")).click();
		    driver.findElement(By.id("NameVal")).click();
		    driver.findElement(By.id("NameZone")).click();
		    driver.findElement(By.id("NameZone")).clear();
		    driver.findElement(By.id("NameZone")).sendKeys("1");
		    driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		    logger.info("Implicit wait_NameZone");
		    
		    
		    Actions act=new Actions(driver);
		    WebElement drag=driver.findElement(By.xpath("//*[@id=\"NameVal\"]"));
		    WebElement drop=driver.findElement(By.xpath("//*[@id=\"zone1\"]"));
		    act.dragAndDrop(drag, drop).build().perform();
		    
		    driver.findElement(By.id("templatebtn1")).click();                                
			
			 WebDriverWait wait5 = new WebDriverWait(driver, 3000);
			 wait5.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"zone1\"]"))).click();
			 logger.info("Explicit wait4");                                
		 
		    driver.findElement(By.id("NameZone")).click();
		    driver.findElement(By.id("NameZone")).clear();
		    driver.findElement(By.id("NameZone")).sendKeys("1");
		    driver.findElement(By.id("templatebtn1")).click();
		    driver.findElement(By.id("zone1")).click();
		    driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
		    
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='View Form Template'])[1]/following::div[71]")).click();
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[1]/preceding::p[2]")).click();
		    driver.findElement(By.id("NameLabelZone")).click();
		    driver.findElement(By.id("NameLabelZone")).clear();
		    driver.findElement(By.id("NameLabelZone")).sendKeys("1");
		    
		    driver.findElement(By.id("zone1")).click();
		    driver.findElement(By.id("NameZone")).click();
		    driver.findElement(By.id("NameZone")).clear();
		    driver.findElement(By.id("NameZone")).sendKeys("1");
		    
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Approver(s)'])[1]/following::p[1]")).click();
		    driver.findElement(By.id("templatebtn1")).click();
		    driver.findElement(By.id("zone1")).click();
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/following::div[2]")).click();
		    driver.findElement(By.id("NameLabel")).click();
		    driver.findElement(By.id("NameZone")).click();
		    driver.findElement(By.id("NameZone")).clear();
		    driver.findElement(By.id("NameZone")).sendKeys("1");
		    
		    driver.findElement(By.id("zone1")).click();
		    driver.findElement(By.id("formName")).click();
		    driver.findElement(By.id("formName")).clear();
		    driver.findElement(By.id("formName")).sendKeys("AppF12");
		    
		    driver.findElement(By.id("templatebtn1")).click();
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Yes'])[1]/following::button[1]")).click();
}
	@After
	  public void tearDown() throws Exception {
	    driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	      fail(verificationErrorString);
	    }
	  }

	  private boolean isElementPresent(By by) {
	    try {
	      driver.findElement(by);
	      return true;
	    } catch (NoSuchElementException e) {
	      return false;
	    }
	  }

	  private boolean isAlertPresent() {
	    try {
	      driver.switchTo().alert();
	      return true;
	    } catch (NoAlertPresentException e) {
	      return false;
	    }
	  }

	  private String closeAlertAndGetItsText() {
	    try {
	      Alert alert = driver.switchTo().alert();
	      String alertText = alert.getText();
	      if (acceptNextAlert) {
	        alert.accept();
	      } else {
	        alert.dismiss();
	      }
	      return alertText;
	    } finally {
	      acceptNextAlert = true;
	    }
	  }
	}
