package testpackage1;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class SaveRequest5 {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();
	@Before
	public void setUp() throws Exception {

		System.setProperty("webdriver.chrome.driver", "D://chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		baseUrl = "https://micloud365.sharepoint.com/sites/appsdevv15";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
}
	 @Test
	  public void testUntitledTestCase() throws Exception {
		 driver.get(baseUrl+"/FlowF31/Pages/index.aspx");
		 
		 //Sharepoint login 
		 driver.findElement(By.id("i0116")).sendKeys("dev-test-02@millenniumitesp.com");

			WebElement a = driver.findElement(By.id("idSIButton9"));
			a.click();
			driver.findElement(By.id("i0118")).sendKeys("e~WR@)<VJ5!q7?2Q");
			WebDriverWait wait1 = new WebDriverWait(driver, 30);
			wait1.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();
			
			  driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='My Actions'])[1]/following::span[2]")).click();
			    driver.findElement(By.id("Name")).click();
			    driver.findElement(By.id("Name")).clear();
			    driver.findElement(By.id("Name")).sendKeys("Sewwandi1");
			   
			    
			    driver.findElement(By.id("No")).click();
			    driver.findElement(By.id("No")).clear();
			    driver.findElement(By.id("No")).sendKeys("47");
			    driver.findElement(By.id("Date")).click();
			    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Sa'])[1]/following::td[17]")).click();
			    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Clear'])[1]/following::span[17]")).click();
			    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Close'])[3]/following::span[16]")).click();
			    
			    driver.findElement(By.xpath("//*[@id=\"formdiv\"]/div/div/div/div")).click();
			    driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
			    
			    driver.findElement(By.xpath("//*[@id=\"btnSaveNew\"]/button")).click();
			    driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
			    
			    
			    driver.findElement(By.id("approverClose")).click();
			    
			   // WebDriverWait wait3 = new WebDriverWait(driver, 300);
			   //wait3.until(ExpectedConditions.elementToBeClickable(By.id("approverClose"))).click();
				
			   //driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
			    
				
				
				//WebDriverWait wait4 = new WebDriverWait(driver, 300);
				//wait4.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"success\"]/div/div/div[3]/button"))).click();
				
				//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				//driver.findElement(By.xpath("//*[@id=\"success\"]/div/div/div[3]/button")).clear();
			    
			    // driver.findElement(By.id("TemplateDiv")).click();
			    //driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Save'])[1]/following::button[1]")).click();
			    //driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Successfull'])[1]/following::button[1]")).click();
			    
			  }

	  

	  @After
	  public void tearDown() throws Exception {
	    driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	      fail(verificationErrorString);
	    }
	  }

	  private boolean isElementPresent(By by) {
	    try {
	      driver.findElement(by);
	      return true;
	    } catch (NoSuchElementException e) {
	      return false;
	    }
	  }

	  private boolean isAlertPresent() {
	    try {
	      driver.switchTo().alert();
	      return true;
	    } catch (NoAlertPresentException e) {
	      return false;
	    }
	  }

	  private String closeAlertAndGetItsText() {
	    try {
	      Alert alert = driver.switchTo().alert();
	      String alertText = alert.getText();
	      if (acceptNextAlert) {
	        alert.accept();
	      } else {
	        alert.dismiss();
	      }
	      return alertText;
	    } finally {
	      acceptNextAlert = true;
	    }
	  }
	}

