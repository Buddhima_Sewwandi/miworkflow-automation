package testpackage1;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WorkFlowCreate3 {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		
		
		Logger logger=Logger.getLogger("log1");
		System.setProperty("webdriver.chrome.driver", "D://chromedriver.exe");
		driver = new ChromeDriver();
		logger.info("Browser Opened");
		
		driver.manage().window().maximize();
		baseUrl = "https://micloud365.sharepoint.com/sites/appsdevv15";
		logger.info("Url opened");
		
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
	}
	
	
	@Test
	public void testWorkFlowCreate() throws Exception {
		Logger logger=Logger.getLogger("log1");
		// Share point login
		driver.get(baseUrl);
		driver.findElement(By.id("i0116")).sendKeys("dev-test-02@millenniumitesp.com");
		
		WebElement a = driver.findElement(By.id("idSIButton9"));
		a.click();
		driver.findElement(By.id("i0118")).sendKeys("e~WR@)<VJ5!q7?2Q");
		WebDriverWait wait1 = new WebDriverWait(driver, 300);
		wait1.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();
		logger.info("Explicit wait1");

		WebDriverWait wait2 = new WebDriverWait(driver, 300);
		wait2.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();
		logger.info("Explicit wait2");

		WebDriverWait wait3 = new WebDriverWait(driver, 300);
		wait3.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myTab\"]/li[2]/a"))).click();
		driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
		logger.info("Explicit wait3");
		

		driver.findElement(By.id("wfTitle")).click();
		driver.findElement(By.id("wfTitle")).clear();
		driver.findElement(By.id("wfTitle")).sendKeys("WorkflowT6588872");
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		logger.info("Implicit wait1_wfTitle");

		driver.findElement(By.id("workflowName")).click();
		driver.findElement(By.id("workflowName")).clear();
		driver.findElement(By.id("workflowName")).sendKeys("WorkflowT6588872");
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		logger.info("Implicit wait2_workflowName");

		
		driver.findElement(By.id("wfDescription")).click();
		driver.findElement(By.id("wfDescription")).clear();
		driver.findElement(By.id("wfDescription")).sendKeys("Workflow6588872");
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		logger.info("Implicit wait3_wfDescription");

		driver.findElement(By.id("workflowImage")).click();
		// driver.findElement(By.id("workflowImage")).clear();
		driver.findElement(By.id("workflowImage")).sendKeys("D:\\index.jpg");
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		logger.info("Implicit wait4_workflowImage");

		WebDriverWait wait4 = new WebDriverWait(driver, 3000);
		wait4.until(ExpectedConditions.elementToBeClickable(By.id("submitWorkflow"))).click();
		logger.info("Explicit wait5_submitWorkflow");

		driver.findElement(By.id("fieldName")).click();
		 driver.findElement(By.id("fieldName")).clear();
		 driver.findElement(By.id("fieldName")).sendKeys("Name");
		 driver.findElement(By.id("fieldType")).click();
		 driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		 logger.info("implicit wait5_fieldName_Name");
		 
		 new Select(driver.findElement(By.id("fieldType"))).selectByVisibleText("Text");
		 driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		 logger.info("implicit wait6_fieldType_Text");
		 
		    driver.findElement(By.id("fieldType")).click();
		    driver.findElement(By.id("addButton")).click();
		    driver.findElement(By.id("fieldName")).click();
		    driver.findElement(By.id("fieldName")).clear();
		    driver.findElement(By.id("fieldName")).sendKeys("Number");
		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.MINUTES);
		    logger.info("Implicit wait7_fieldName_Number");
		    
		    driver.findElement(By.id("fieldType")).click();
		    new Select(driver.findElement(By.id("fieldType"))).selectByVisibleText("Number");
		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS); 
		    logger.info("Implicit wait8_fieldType_Number");
		    
		    driver.findElement(By.id("fieldType")).click();
		    driver.findElement(By.id("addButton")).click();
		    driver.findElement(By.id("nextButton")).click();
		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		    logger.info("Implicit wait9_nextButton");
		    
		    driver.findElement(By.id("NameDefault")).click();
		    driver.findElement(By.id("NameDefault")).clear();
		    driver.findElement(By.id("NameDefault")).sendKeys("Name");
		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS); 
		    logger.info("Implicit wait10_NameDefault");
		    
		    driver.findElement(By.id("NumberDefault")).click();
		    driver.findElement(By.id("NumberDefault")).clear();
		    driver.findElement(By.id("NumberDefault")).sendKeys("0");
		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS); 
		    logger.info("Implicit wait11_NumberDefault");
		    
		    driver.findElement(By.id("NumberMin")).click();
		    driver.findElement(By.id("NumberMin")).clear();
		    driver.findElement(By.id("NumberMin")).sendKeys("1");
		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS); 
		    logger.info("Implicit wait12_NumberMin");
		    
		    driver.findElement(By.id("NumberMax")).click();
		    driver.findElement(By.id("NumberMax")).clear();
		    driver.findElement(By.id("NumberMax")).sendKeys("100");
		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS); 
		    logger.info("Implicit wait13_NumberMax");
		    
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/i[1]")).click();
		    driver.findElement(By.id("zone1")).click();
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/following::div[2]")).click();
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='References'])[1]/following::label[1]")).click();
		    driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		    logger.info("Implicit wait14_Selection of Zone Template");
		    
		    
		    driver.findElement(By.id("NameZone")).click();
		    driver.findElement(By.id("NameZone")).clear();
		    driver.findElement(By.id("NameZone")).sendKeys("1");
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Approver(s)'])[1]/following::span[2]")).click();
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='References'])[1]/following::label[1]")).click();
		   
		   
		   
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Approver(s)'])[1]/following::span[2]")).click();
		    driver.findElement(By.id("NumberVal")).click();
		    driver.findElement(By.id("NumberZone")).click();
		    driver.findElement(By.id("NumberZone")).clear();
		    driver.findElement(By.id("NumberZone")).sendKeys("2");
		    //driver.findElement(By.id("templatebtn1")).click();
		    
		 
		    
		    driver.findElement(By.id("templatebtn1")).click();
		    driver.findElement(By.id("zone1")).click();
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='View Form Template'])[1]/following::div[71]")).click();
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[1]/preceding::p[4]")).click();
		    driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
		    
		    driver.findElement(By.id("NameLabelZone")).click();
		    driver.findElement(By.id("NameLabelZone")).clear();
		    driver.findElement(By.id("NameLabelZone")).sendKeys("1");
		    driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
		    
		    driver.findElement(By.id("controllsSettingDivView")).click();
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='References'])[1]/following::label[1]")).click();
		    driver.findElement(By.id("NumberZone")).click();
		    driver.findElement(By.id("NumberZone")).clear();
		    driver.findElement(By.id("NumberZone")).sendKeys("1");
		    
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Approver(s)'])[1]/following::p[1]")).click();
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Approver(s)'])[1]/following::p[1]")).click();
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Approver(s)'])[1]/following::p[4]")).click();
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Approver(s)'])[1]/following::p[4]")).click();
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Approver(s)'])[1]/following::span[2]")).click();
		    driver.findElement(By.id("NumbermainValView")).click();
		    
		    driver.findElement(By.id("NumberLabelZone")).click();
		    driver.findElement(By.id("NumberLabelZone")).clear();
		    driver.findElement(By.id("NumberLabelZone")).sendKeys("2");
		    
		    driver.findElement(By.id("NumberZone")).click();
		    driver.findElement(By.id("NumberZone")).clear();
		    driver.findElement(By.id("NumberZone")).sendKeys("2");
		    
		    driver.findElement(By.id("zone2")).click();
		    
		    driver.findElement(By.id("templatebtn1")).click();
		    driver.findElement(By.id("zone1")).click();
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/following::div[2]")).click();
		    
		    driver.findElement(By.id("NameMain")).click();
		    
		    driver.findElement(By.id("NameZone")).click();
		    driver.findElement(By.id("NameZone")).clear();
		    driver.findElement(By.id("NameZone")).sendKeys("1");
		    driver.findElement(By.id("zone1")).click();
		    
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Form Name'])[1]/following::p[4]")).click();
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Approver(s)'])[1]/following::span[2]")).click();
		    
		    driver.findElement(By.id("NumberLabel")).click();
		    driver.findElement(By.id("NumberZone")).click();
		    driver.findElement(By.id("NumberZone")).clear();
		    driver.findElement(By.id("NumberZone")).sendKeys("2");
		    
		    driver.findElement(By.id("zone2")).click();
		    driver.findElement(By.id("formName")).click();
		    driver.findElement(By.id("formName")).clear();
		    driver.findElement(By.id("formName")).sendKeys("FormT543");
		    driver.findElement(By.id("templatebtn1")).click();
		    
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Yes'])[1]/following::button[1]")).click();
		    driver.findElement(By.id("LevelType")).click();
		    
		    new Select(driver.findElement(By.id("LevelType"))).selectByVisibleText("Sequential");
		    driver.findElement(By.id("LevelType")).click();
		    
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Add new level'])[1]/following::button[1]")).click();
		    driver.findElement(By.id("addNewApproverType")).click();
		    driver.findElement(By.id("addNewApproverType")).click();
		    driver.findElement(By.id("settingsLabels")).click();
		    driver.findElement(By.id("addNewApproverType")).click();
		    
		    new Select(driver.findElement(By.id("addNewApproverType"))).selectByVisibleText("Normal approver");
		    driver.findElement(By.id("addNewApproverType")).click();
		    
		    driver.findElement(By.id("ctl00_PlaceHolderMain_addNewApproverUser_upLevelDiv")).click();
		    // ERROR: Caught exception [unknown command [editContent]]
		    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='DEV-TEST-03'])[1]/following::button[1]")).click();
		    Assert.assertEquals("Approver successfuly added", closeAlertAndGetItsText());
		    
		  }

		  @After
		  public void tearDown() throws Exception {
		    driver.quit();
		    String verificationErrorString = verificationErrors.toString();
		    if (!"".equals(verificationErrorString)) {
		      fail(verificationErrorString);
		    }
		  }

		  private boolean isElementPresent(By by) {
		    try {
		      driver.findElement(by);
		      return true;
		    } catch (NoSuchElementException e) {
		      return false;
		    }
		  }

		  private boolean isAlertPresent() {
		    try {
		      driver.switchTo().alert();
		      return true;
		    } catch (NoAlertPresentException e) {
		      return false;
		    }
		  }

		  private String closeAlertAndGetItsText() {
		    try {
		      Alert alert = driver.switchTo().alert();
		      String alertText = alert.getText();
		      if (acceptNextAlert) {
		        alert.accept();
		      } else {
		        alert.dismiss();
		      }
		      return alertText;
		    } finally {
		      acceptNextAlert = true;
		    }
		  }
		}

		    