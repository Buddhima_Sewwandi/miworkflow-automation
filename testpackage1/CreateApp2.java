package testpackage1;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateApp2 {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {

		System.setProperty("webdriver.chrome.driver", "D://chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		baseUrl = "https://micloud365.sharepoint.com/sites/appsdevv15";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	 @Test
	  public void testUntitledTestCase() throws Exception {
		// Share point login
			driver.get(baseUrl);
			driver.findElement(By.id("i0116")).sendKeys("dev-test-02@millenniumitesp.com");

			WebElement a = driver.findElement(By.id("idSIButton9"));
			a.click();
			driver.findElement(By.id("i0118")).sendKeys("e~WR@)<VJ5!q7?2Q");
			WebDriverWait wait1 = new WebDriverWait(driver, 30);
			wait1.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();

			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();

			WebDriverWait wait3 = new WebDriverWait(driver, 300);
			wait3.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"myTab\"]/li[2]/a"))).click();
			driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
			
	    driver.findElement(By.linkText("Create App")).click();
	    
	    driver.findElement(By.id("wfTitle")).click();
	    driver.findElement(By.id("wfTitle")).clear();
	    driver.findElement(By.id("wfTitle")).sendKeys("FlowTest7722");
	    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
	    
	    driver.findElement(By.id("workflowName")).click();
	    driver.findElement(By.id("workflowName")).clear();
	    driver.findElement(By.id("workflowName")).sendKeys("FlowTest7722");
	    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
	    
	    driver.findElement(By.id("wfDescription")).click();
	    driver.findElement(By.id("wfDescription")).clear();
	    driver.findElement(By.id("wfDescription")).sendKeys("FlowTest7722");
	    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
	    
	    driver.findElement(By.id("workflowImage")).click();
	    driver.findElement(By.id("workflowImage")).clear();
	    driver.findElement(By.id("workflowImage")).sendKeys("D:\\index.jpg");
	    
	   // driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Workflow Url Name'])[1]/following::label[1]")).click();
	   // driver.findElement(By.id("submitWorkflow")).click();

	    WebDriverWait wait4 = new WebDriverWait(driver, 300);
		wait4.until(ExpectedConditions.elementToBeClickable(By.id("submitWorkflow"))).click();
		//driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		// driver.findElement(By.id("fieldName")).click();
		wait4.until(ExpectedConditions.elementToBeClickable(By.id("fieldName"))).click();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.findElement(By.id("fieldName")).clear();
}
	 @After
	  public void tearDown() throws Exception {
	    driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	      fail(verificationErrorString);
	    }
	  }

	  private boolean isElementPresent(By by) {
	    try {
	      driver.findElement(by);
	      return true;
	    } catch (NoSuchElementException e) {
	      return false;
	    }
	  }

	  private boolean isAlertPresent() {
	    try {
	      driver.switchTo().alert();
	      return true;
	    } catch (NoAlertPresentException e) {
	      return false;
	    }
	  }

	  private String closeAlertAndGetItsText() {
	    try {
	      Alert alert = driver.switchTo().alert();
	      String alertText = alert.getText();
	      if (acceptNextAlert) {
	        alert.accept();
	      } else {
	        alert.dismiss();
	      }
	      return alertText;
	    } finally {
	      acceptNextAlert = true;
	    }
	  }
	}


