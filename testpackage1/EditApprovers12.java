package testpackage1;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditApprovers12 {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();
	@Before
	public void setUp() throws Exception {

		System.setProperty("webdriver.chrome.driver", "D://chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		baseUrl = "https://micloud365.sharepoint.com/sites/appsdevv15";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
}
	@Test
	  public void testUntitledTestCase() throws Exception {
		driver.get(baseUrl+"/FlowTEList/Pages/index.aspx");
		
		//sharepoint login
		 driver.findElement(By.id("i0116")).sendKeys("dev-test-02@millenniumitesp.com");

			WebElement a = driver.findElement(By.id("idSIButton9"));
			a.click();
			driver.findElement(By.id("i0118")).sendKeys("e~WR@)<VJ5!q7?2Q");
			WebDriverWait wait1 = new WebDriverWait(driver, 30);
			wait1.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();
			
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			wait2.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();
			
			 driver.findElement(By.linkText("Edit Approvers")).click();
			 driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Workflow Reports'])[1]/following::span[1]")).click();
			    driver.findElement(By.id("approveTitle")).click();
			    driver.findElement(By.id("approveTitle")).clear();
			    driver.findElement(By.id("approveTitle")).sendKeys("AppT45");
			    driver.findElement(By.id("peoplepickerdivApprover_TopSpan_InitialHelpText")).click();
			    driver.findElement(By.id("peoplepickerdivApprover_TopSpan_EditorInput")).clear();
			    driver.findElement(By.id("peoplepickerdivApprover_TopSpan_EditorInput")).sendKeys("dev-test-02");
			    driver.findElement(By.id("peoplepickerdivApprover_TopSpan_EditorInput")).sendKeys(Keys.ENTER);
			    driver.findElement(By.id("level")).click();
			    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Suggestions are available. Use up and down arrows to select.'])[1]/following::div[89]")).click();
	}
	 @After
	  public void tearDown() throws Exception {
	    driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	      fail(verificationErrorString);
	    }
	  }

	  private boolean isElementPresent(By by) {
	    try {
	      driver.findElement(by);
	      return true;
	    } catch (NoSuchElementException e) {
	      return false;
	    }
	  }

	  private boolean isAlertPresent() {
	    try {
	      driver.switchTo().alert();
	      return true;
	    } catch (NoAlertPresentException e) {
	      return false;
	    }
	  }

	  private String closeAlertAndGetItsText() {
	    try {
	      Alert alert = driver.switchTo().alert();
	      String alertText = alert.getText();
	      if (acceptNextAlert) {
	        alert.accept();
	      } else {
	        alert.dismiss();
	      }
	      return alertText;
	    } finally {
	      acceptNextAlert = true;
	    }
	  }
	}


	
			