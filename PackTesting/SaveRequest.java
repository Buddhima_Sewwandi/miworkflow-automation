package PackTesting;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SaveRequest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();


  @Test
  public void testSaveRequest() throws Exception {
    driver.get( baseUrl+"FlowF31/Pages/index.aspx");
    
    driver.findElement(By.id("i0116")).sendKeys("dev-test-02@millenniumitesp.com");
	
	WebElement a=driver.findElement(By.id("idSIButton9"));
    a.click();
    driver.findElement(By.id("i0118")).sendKeys("e~WR@)<VJ5!q7?2Q");
    WebDriverWait wait1 = new    WebDriverWait(driver,30);
    wait1.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();
    
    WebDriverWait wait2 = new    WebDriverWait(driver,30);
    wait2.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();
    
    
    WebDriverWait wait3 = new    WebDriverWait(driver,300);
    wait3.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul[@id='myTab']/li[3]/a/span"))).click();
    driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
    
    driver.findElement(By.id("Name")).click();
    driver.findElement(By.id("Name")).clear();
    driver.findElement(By.id("Name")).sendKeys("kasun");
    
    WebDriverWait wait5 = new    WebDriverWait(driver,300);
    wait5.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"btnSaveNew\"]/button"))).click();
    wait5.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"success\"]/div/div/div[3]/button"))).click();
    
   // driver.findElement(By.xpath("(//button[@type='button'])[58]")).click();
    //driver.findElement(By.xpath("(//button[@type='button'])[26]")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
