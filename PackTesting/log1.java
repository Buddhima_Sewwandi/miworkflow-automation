package PackTesting;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class log1 {
	private static final String PropertyConfigurator = null;
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();
	@Before
	public void setUp() throws Exception {
        
		Logger logger=Logger.getLogger("log1");
		
		System.setProperty("webdriver.chrome.driver", "D://chromedriver.exe");
		driver = new ChromeDriver();
		logger.info("Browser Opened");
		driver.manage().window().maximize();
		baseUrl = "https://micloud365.sharepoint.com/sites/appsdevv15";
		logger.info("Url opened");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
}
	//testUntitledTestCase
		@Test
		public void TestClass1() throws Exception {
			Logger logger=Logger.getLogger("log1");
	      
		  driver.get("https://micloud365.sharepoint.com/sites/appsdevv15/");
		  
		  
			//driver.get(baseUrl + "https://micloud365.sharepoint.com/sites/appsdevv15/Pages/miWorkflow.aspx");

			driver.findElement(By.id("i0116")).sendKeys("dev-test-02@millenniumitesp.com");

			WebElement a = driver.findElement(By.id("idSIButton9"));
			a.click();
			driver.findElement(By.id("i0118")).sendKeys("e~WR@)<VJ5!q7?2Q");
			WebDriverWait wait1 = new WebDriverWait(driver, 30);
			logger.info("Explicit wait1");
			
			wait1.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			logger.info("Explicit wait2");
			
			wait2.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();
			
			
		}
			@After
			  public void tearDown() throws Exception {
			    driver.quit();
			    String verificationErrorString = verificationErrors.toString();
			    if (!"".equals(verificationErrorString)) {
			      fail(verificationErrorString);
			    }
			  }

			  private boolean isElementPresent(By by) {
			    try {
			      driver.findElement(by);
			      return true;
			    } catch (NoSuchElementException e) {
			      return false;
			    }
			  }

			  private boolean isAlertPresent() {
			    try {
			      driver.switchTo().alert();
			      return true;
			    } catch (NoAlertPresentException e) {
			      return false;
			    }
			  }

			  private String closeAlertAndGetItsText() {
			    try {
			      Alert alert = driver.switchTo().alert();
			      String alertText = alert.getText();
			      if (acceptNextAlert) {
			        alert.accept();
			      } else {
			        alert.dismiss();
			      }
			      return alertText;
			    } finally {
			      acceptNextAlert = true;
			    }
			  }
			}



