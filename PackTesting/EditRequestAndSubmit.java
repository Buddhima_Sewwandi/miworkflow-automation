package PackTesting;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditRequestAndSubmit{
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
	  
	   System.setProperty("webdriver.chrome.driver", "D://chromedriver.exe");
	   driver = new ChromeDriver();
	   baseUrl = "https://micloud365.sharepoint.com/sites/appsqav12";
       driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testEditRequest() throws Exception {
	  
	    driver.get(baseUrl+"/Auto100/Pages/index.aspx");
	    driver.findElement(By.id("i0116")).sendKeys("maheshikad@millenniumitesp.com");
	
	    try {
		WebElement a=driver.findElement(By.id("idSIButton9"));
	    a.click();
	    driver.findElement(By.id("i0118")).sendKeys("mit_0986m");
	    WebDriverWait wait1 = new    WebDriverWait(driver,30);
	    wait1.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();
	    
	    WebDriverWait wait2 = new    WebDriverWait(driver,30);
	    wait2.until(ExpectedConditions.elementToBeClickable(By.id("idSIButton9"))).click();

	    } catch(Exception e){
	    
	    
	    
	    WebDriverWait wait3 = new    WebDriverWait(driver,300);
	    wait3.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"borderLayout_eGridPanel\"]/div[1]/div/div[4]/div[3]/div/div/div[13]/div[5]/div/button[2]/span"))).click();
	    }
	    
	   // driver.findElement(By.xpath("//div[@id='borderLayout_eGridPanel']/div/div/div[4]/div[3]/div/div/div[12]/div[5]/div/button[2]/i")).click();
	    driver.findElement(By.id("NameUpdate")).click();
	    driver.findElement(By.id("NameUpdate")).clear();
	    driver.findElement(By.id("NameUpdate")).sendKeys("Kasun Sammera");
	    driver.findElement(By.xpath("(//button[@type='button'])[93]")).click();
	    driver.findElement(By.xpath("//div[@id='success']/div/div/div[2]")).click();
	    
	  
	  
   /* driver.findElement(By.xpath("//div[@id='borderLayout_eGridPanel']/div/div/div[4]/div[3]/div/div/div[8]/div[5]/div/button[2]/i")).click();
    driver.findElement(By.id("NameUpdate")).click();
    driver.findElement(By.id("NameUpdate")).clear();
    driver.findElement(By.id("NameUpdate")).sendKeys("kasun Sameera");
    driver.findElement(By.xpath("(//button[@type='button'])[84]")).click();
    driver.findElement(By.xpath("(//button[@type='button'])[62]")).click();
    
    
    
    
    /*
         
     */
    
    
    
    
   
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
